#!/bin/bash
######################################################
clear

cd Code/

echo 'scraping the URL and downloading statistics of NBA players ...'
sleep 3
python url_download.py
clear
######################################################

echo 'Extracting, Cleaning and Saving the data in tables ...'
sleep 3
python ETL.py -f
clear
 
######################################################

echo 'Changing file names to upload to GCP ....'
sleep 3

cd data/tables/tb_player_defense
for filename in *.csv
do
  mv "$filename" "tb_player_defense.csv"
done

cd ..
cd tb_player_field_goals
for filename in *.csv
do
  mv "$filename" "tb_player_field_goals.csv"
done

cd ..
cd tb_player_free_throws
for filename in *.csv
do
  mv "$filename" "tb_player_free_throws.csv"
done

cd ..
cd tb_player_info
for filename in *.csv
do
  mv "$filename" "tb_player_info.csv"
done

cd ..
cd tb_players
for filename in *.csv
do
  mv "$filename" "tb_players.csv"
done
  
cd ..
cd tb_player_three_pointers
for filename in *.csv
do
  mv "$filename" "tb_player_three_pointers.csv"

clear
######################################################

sleep 3
cd ..
cd ..
cd ..

echo '''
1 - Upload to GCP Storage.
2 - Upload to GCP BigQuery.
3 - Upload to both.

Select the option (1, 2, 3): '''
read option

while [ $option != "1" ] && [ $option != "2" ] && [ $option != "3" ]
do
	clear
	echo 'Invalid Choice..'
	echo '''
1 - Upload to GCP Storage.
2 - Upload to GCP BigQuery.
3 - Upload to both.

Select the option (1, 2, 3): '''
	read option
done

if [ $option = "1" ]
then
	python upload_storage.py
	clear
	echo 'Upload to Storage Complete !!'
elif [ $option = "2" ]
then
	python upload_bigquery.py
	clear
	echo 'Upload to Storage BigQuery Complete !!'
elif [ $option = "3" ] 
then
	python upload_storage.py
	python upload_bigquery.py
	echo 'Upload Completo !!'
fi

done
