try:
    import os
    import scrapy
    from scrapy.crawler import CrawlerProcess
except Exception as e:
    print("Some Modules are Missing {}".format(e))

# Create an empty list to store urls
list_urls = []

# Set the year range to begin and page to begin
year = 2010
page = 1

# Using a for loop, create a list of urls for each page and year
for year in range(year,2024):
    for page in range(1,4):
        url = "https://basketball.realgm.com/nba/stats/"+str(year)+"/Averages/Qualified/points/All/desc/"+str(page)+"/Regular_Season"
        list_urls.append(url)
        page += 1
    year += 1

# Define the class for the spider
class NBA(scrapy.Spider):
    name = 'nba'

    start_urls = list_urls

    # Create a parse method to scrape the data
    def parse(self, response):
        for i in range(100):
            yield {
                'rank': response.css('.rank::text')[i].get(),
                'player': response.css('.nowrap a::text')[i].get(),
                'team': response.css('.nowrap+ td::text')[i].get(),
                'games_played': response.css('td:nth-child(4)::text')[i].get(),
                'minutes_per_game': response.css('td:nth-child(5)::text')[i].get(),
                'points_per_game': response.css('td:nth-child(6)::text')[i].get(),
                'field_goals_made': response.css('td:nth-child(7)::text')[i].get(),
                'field_goals_attempted': response.css('td:nth-child(8)::text')[i].get(),
                'field_goal_percentage': response.css('td:nth-child(9)::text')[i].get(),
                'three_pointers_made': response.css('td:nth-child(10)::text')[i].get(),
                'three_pointers_attempted': response.css('td:nth-child(11)::text')[i].get(),
                'three_pointers_percentage': response.css('td:nth-child(12)::text')[i].get(),
                'free_throws_made': response.css('td:nth-child(13)::text')[i].get(),
                'free_throws_attempted': response.css('td:nth-child(14)::text')[i].get(),
                'free_throws_percentage': response.css('td:nth-child(15)::text')[i].get(),
                'offensive_rebounds': response.css('td:nth-child(16)::text')[i].get(),
                'defensive_rebounds': response.css('td:nth-child(17)::text')[i].get(),
                'rebounds_per_game': response.css('td:nth-child(18)::text')[i].get(),
                'assists_per_game': response.css('td:nth-child(19)::text')[i].get(),
                'steals_per_game': response.css('td:nth-child(20)::text')[i].get(),
                'blocks_per_game': response.css('td:nth-child(21)::text')[i].get(),
                'turnovers_per_game': response.css('td:nth-child(22)::text')[i].get(),
                'fouls_per_game': response.css('td:nth-child(23)::text')[i].get(),
                'year' : response.css('.page_title::text').get()
            }
        pass

# Set the file name and path
FILE_NAME = 'data/nba.csv'

# If a file already exists with the same name, remove it
if os.path.exists(FILE_NAME):
    os.remove(FILE_NAME)

# Create a settings variable with the settings for the crawler
SETTINGS = {
            'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)', 
            'FEED_FORMAT': 'csv', 
            'FEED_URI': FILE_NAME, 
            'CONCURRENT_ITEMS': 1 
            }

# Create the crawler process
process = CrawlerProcess(SETTINGS)

# Start the crawler by passing in the Spider class
process.crawl(NBA)
process.start()