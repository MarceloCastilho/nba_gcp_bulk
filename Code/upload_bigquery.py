# Try to import the necessary modules (pandas, google.cloud and google.oauth2). 
# If some module is missing, print an error message
try:
    import pandas as pd
    from google.cloud import bigquery
    from google.oauth2 import service_account
except Exception as e:
    print("Some Modules are Missing {}".format(e))

# Load the data from the CSV files
tb_player_defense = pd.read_csv("data/tables/tb_player_defense/tb_player_defense.csv")
tb_player_field_goals = pd.read_csv("data/tables/tb_player_field_goals/tb_player_field_goals.csv")
tb_player_free_throws = pd.read_csv("data/tables/tb_player_free_throws/tb_player_free_throws.csv")
tb_player_info = pd.read_csv("data/tables/tb_player_info/tb_player_info.csv")
tb_player_three_pointers = pd.read_csv("data/tables/tb_player_three_pointers/tb_player_three_pointers.csv")
tb_players = pd.read_csv("data/tables/tb_players/tb_players.csv")


# Set the path for the service account key
# Set the credentials of the service account
key_path = "/home/marcelo/GCP/nba-projeto-bulk-12f049d54bf9.json"
credentials = service_account.Credentials.from_service_account_file(key_path)

# Upload the data to Google BigQuery, 
# replacing the data if the table already exists
tb_player_defense.to_gbq(credentials=credentials,
destination_table="nba.tb_player_defense", if_exists='replace')
tb_player_field_goals.to_gbq(credentials=credentials,
destination_table="nba.tb_player_field_goals", if_exists='replace')
tb_player_free_throws.to_gbq(credentials=credentials,
destination_table="nba.tb_player_free_throws", if_exists='replace')
tb_player_info.to_gbq(credentials=credentials,
destination_table="nba.tb_player_info", if_exists='replace')
tb_player_three_pointers.to_gbq(credentials=credentials,
destination_table="nba.tb_player_three_pointers", if_exists='replace')
tb_players.to_gbq(credentials=credentials,
destination_table="nba.tb_players", if_exists='replace')
