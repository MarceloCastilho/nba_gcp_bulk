# Import the necessary libraries
try:
    from pyspark.sql import SparkSession
    from pyspark.sql.functions import *
    from pyspark.sql.types import *
except Exception as e:
    print("Some Modules are Missing {}".format(e))

# Create the SparkSession
spark = SparkSession.builder \
    .appName("nba_etl")\
    .getOrCreate()

## Read the csv file and drop any rows with missing values
df = spark.read.csv("data/nba.csv", header=True, sep=',', inferSchema=True)
df = df.na.drop()

## Create a distinct list of players and assign each player an ID
players = df.select(col("player")).distinct()
ID_players = players.withColumn('ID_player', monotonically_increasing_id())\
                    .withColumnRenamed("player", "players")

## Join the players table with the main dataframe and rename the year column
df1 = df.join(ID_players, df.player == ID_players.players, "inner").drop("players")
df2 = df1.withColumn("year", regexp_extract("year", r'(\d{4}-\d{4})', 1))\
            .withColumnRenamed("year", "season")

## Create a table for players, their info, field goals, three pointers, free throws and defense
tb_players = ID_players.withColumnRenamed("players", "player")\
                        .select("ID_player", "player")
tb_player_info = df2.select("ID_player", "team", "points_per_game", "games_played",
"minutes_per_game", "turnovers_per_game", "assists_per_game", "rebounds_per_game", "season")
tb_player_field_goals = df2.select("ID_player", "field_goals_made", "field_goals_attempted", 
"field_goal_percentage", "season")
tb_player_three_pointers = df2.select("ID_player", "three_pointers_made", "three_pointers_attempted",
"three_pointers_percentage", "season")
tb_player_free_throws = df2.select("ID_player", "free_throws_made", "free_throws_attempted",
"free_throws_percentage", "season")
tb_player_defense = df2.select("ID_player", "offensive_rebounds", "defensive_rebounds",
 "rebounds_per_game", "steals_per_game", "blocks_per_game", "season")

## Write the tables to csv files
tb_players.write.csv("data/tables/tb_players", mode="overwrite", header=True)
tb_player_info.write.csv("data/tables/tb_player_info", mode="overwrite", header=True)
tb_player_field_goals.write.csv("data/tables/tb_player_field_goals", mode="overwrite", header=True)
tb_player_three_pointers.write.csv("data/tables/tb_player_three_pointers", mode="overwrite", header=True)
tb_player_free_throws.write.csv("data/tables/tb_player_free_throws", mode="overwrite", header=True)
tb_player_defense.write.csv("data/tables/tb_player_defense", mode="overwrite", header=True)

