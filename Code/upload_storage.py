try: 
    import pandas as pd
    from io import BytesIO
    import io
    from google.cloud import storage
except Exception as e:
    print("Some Modules are Missing {}".format(e))


storage_client = storage.Client.from_service_account_json("/home/marcelo/GCP//nba-projeto-bulk-12f049d54bf9.json")

# create a Bucket object
bucket = storage_client.get_bucket("nba-dados")

###################################

filename = "tb_player_defense.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_player_defense/tb_player_defense.csv", "rb") as a:
    blob.upload_from_file(a)

###################################

filename = "tb_player_field_goals.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_player_field_goals/tb_player_field_goals.csv", "rb") as a:
    blob.upload_from_file(a)

###################################

filename = "tb_player_free_throws.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_player_free_throws/tb_player_free_throws.csv", "rb") as a:
    blob.upload_from_file(a)

###################################

filename = "tb_player_info.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_player_info/tb_player_info.csv", "rb") as a:
    blob.upload_from_file(a)

###################################

filename = "tb_players.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_players/tb_players.csv", "rb") as a:
    blob.upload_from_file(a)

###################################

filename = "tb_player_three_pointers.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_player_three_pointers/tb_player_three_pointers.csv", "rb") as a:
    blob.upload_from_file(a)