# Data Engineer Jr Training Project.


## This project incorporates various technologies utilized in BULK Consulting's talent program.

### The project consists of downloading various data from a website, processing it and sending it to Google Cloud. And now I'll show  how I did it...

#### 1 step: URL Download
website used: https://basketball.realgm.com/nba/stats 

Scrapy is an open source web scraping framework written in Python. It is used to extract data from websites. This code uses scrapy to scrape data from NBA stats website and store it in a CSV file. It uses a for loop to create a list of URLs for each page and year, and then defines a Spider class for the spider. It creates a parse method to scrape the data, and sets the file name and path. It then creates a settings variable with the settings for the crawler, and creates the crawler process. Finally, it starts the crawler by passing in the Spider class.

code below...


```ruby
try:
    import os
    import scrapy
    from scrapy.crawler import CrawlerProcess
except Exception as e:
    print("Some Modules are Missing {}".format(e))

# Create an empty list to store urls
list_urls = []

# Set the year range to begin and page to begin
year = 2010
page = 1

# Using a for loop, create a list of urls for each page and year
for year in range(year,2024):
    for page in range(1,4):
        url = "https://basketball.realgm.com/nba/stats/"+str(year)+"/Averages/Qualified/points/All/desc/"+str(page)+"/Regular_Season"
        list_urls.append(url)
        page += 1
    year += 1

# Define the class for the spider
class NBA(scrapy.Spider):
    name = 'nba'

    start_urls = list_urls

    # Create a parse method to scrape the data
    def parse(self, response):
        for i in range(100):
            yield {
                'rank': response.css('.rank::text')[i].get(),
                'player': response.css('.nowrap a::text')[i].get(),
                'team': response.css('.nowrap+ td::text')[i].get(),
                'games_played': response.css('td:nth-child(4)::text')[i].get(),
                'minutes_per_game': response.css('td:nth-child(5)::text')[i].get(),
                'points_per_game': response.css('td:nth-child(6)::text')[i].get(),
                'field_goals_made': response.css('td:nth-child(7)::text')[i].get(),
                'field_goals_attempted': response.css('td:nth-child(8)::text')[i].get(),
                'field_goal_percentage': response.css('td:nth-child(9)::text')[i].get(),
                'three_pointers_made': response.css('td:nth-child(10)::text')[i].get(),
                'three_pointers_attempted': response.css('td:nth-child(11)::text')[i].get(),
                'three_pointers_percentage': response.css('td:nth-child(12)::text')[i].get(),
                'free_throws_made': response.css('td:nth-child(13)::text')[i].get(),
                'free_throws_attempted': response.css('td:nth-child(14)::text')[i].get(),
                'free_throws_percentage': response.css('td:nth-child(15)::text')[i].get(),
                'offensive_rebounds': response.css('td:nth-child(16)::text')[i].get(),
                'defensive_rebounds': response.css('td:nth-child(17)::text')[i].get(),
                'rebounds_per_game': response.css('td:nth-child(18)::text')[i].get(),
                'assists_per_game': response.css('td:nth-child(19)::text')[i].get(),
                'steals_per_game': response.css('td:nth-child(20)::text')[i].get(),
                'blocks_per_game': response.css('td:nth-child(21)::text')[i].get(),
                'turnovers_per_game': response.css('td:nth-child(22)::text')[i].get(),
                'fouls_per_game': response.css('td:nth-child(23)::text')[i].get(),
                'year' : response.css('.page_title::text').get()
            }
        pass

# Set the file name and path
FILE_NAME = 'data/nba.csv'

# If a file already exists with the same name, remove it
if os.path.exists(FILE_NAME):
    os.remove(FILE_NAME)

# Create a settings variable with the settings for the crawler
SETTINGS = {
            'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)', 
            'FEED_FORMAT': 'csv', 
            'FEED_URI': FILE_NAME, 
            'CONCURRENT_ITEMS': 1 
            }

# Create the crawler process
process = CrawlerProcess(SETTINGS)

# Start the crawler by passing in the Spider class
process.crawl(NBA)
process.start()
```

#### 2 step: Data Modeling Creation

![Semantic description of image](/Images/modelagem.png "Image Title")


#### 3 step: ETL
This code is an example of an ETL (extract, transform and load) pipeline for NBA data. It reads in a csv file containing player stats, creates a table of distinct players and assigns each player an ID, joins the players table with the main dataframe, and then creates 6 tables containing player info, field goals, three pointers, free throws and defense stats. Finally, the tables are written to csv files.

code below...

```ruby
# Import the necessary libraries
try:
    from pyspark.sql import SparkSession
    from pyspark.sql.functions import *
    from pyspark.sql.types import *
except Exception as e:
    print("Some Modules are Missing {}".format(e))

# Create the SparkSession
spark = SparkSession.builder \
    .appName("nba_etl")\
    .getOrCreate()

## Read the csv file and drop any rows with missing values
df = spark.read.csv("data/nba.csv", header=True, sep=',', inferSchema=True)
df = df.na.drop()

## Create a distinct list of players and assign each player an ID
players = df.select(col("player")).distinct()
ID_players = players.withColumn('ID_player', monotonically_increasing_id())\
                    .withColumnRenamed("player", "players")

## Join the players table with the main dataframe and rename the year column
df1 = df.join(ID_players, df.player == ID_players.players, "inner").drop("players")
df2 = df1.withColumn("year", regexp_extract("year", r'(\d{4}-\d{4})', 1))\
            .withColumnRenamed("year", "season")

## Create a table for players, their info, field goals, three pointers, free throws and defense
tb_players = ID_players.withColumnRenamed("players", "player")\
                        .select("ID_player", "player")
tb_player_info = df2.select("ID_player", "team", "points_per_game", "games_played",
"minutes_per_game", "turnovers_per_game", "assists_per_game", "rebounds_per_game", "season")
tb_player_field_goals = df2.select("ID_player", "field_goals_made", "field_goals_attempted", 
"field_goal_percentage", "season")
tb_player_three_pointers = df2.select("ID_player", "three_pointers_made", "three_pointers_attempted",
"three_pointers_percentage", "season")
tb_player_free_throws = df2.select("ID_player", "free_throws_made", "free_throws_attempted",
"free_throws_percentage", "season")
tb_player_defense = df2.select("ID_player", "offensive_rebounds", "defensive_rebounds",
 "rebounds_per_game", "steals_per_game", "blocks_per_game", "season")

## Write the tables to csv files
tb_players.write.csv("data/tables/tb_players", mode="overwrite", header=True)
tb_player_info.write.csv("data/tables/tb_player_info", mode="overwrite", header=True)
tb_player_field_goals.write.csv("data/tables/tb_player_field_goals", mode="overwrite", header=True)
tb_player_three_pointers.write.csv("data/tables/tb_player_three_pointers", mode="overwrite", header=True)
tb_player_free_throws.write.csv("data/tables/tb_player_free_throws", mode="overwrite", header=True)
tb_player_defense.write.csv("data/tables/tb_player_defense", mode="overwrite", header=True)
```

#### 4.1 step: Upload to Google Cloud Storage
The code below is used to upload files from a local folder to a Google Cloud Storage bucket. The code imports the necessary modules, such as pandas and Google Cloud Storage, and then creates a Storage Client from the service account credentials. The code then creates a Bucket object and iterates through a list of files to upload them to the bucket. For each file, the code opens the file, creates a Blob object, and then uploads the file to the bucket.

code below...

```ruby
try: 
    import pandas as pd
    from io import BytesIO
    import io
    from google.cloud import storage
except Exception as e:
    print("Some Modules are Missing {}".format(e))


storage_client = storage.Client.from_service_account_json("/home/marcelo/GCP//nba-projeto-bulk-12f049d54bf9.json")

# create a Bucket object
bucket = storage_client.get_bucket("nba-dados")

###################################

filename = "tb_player_defense.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_player_defense/tb_player_defense.csv", "rb") as a:
    blob.upload_from_file(a)

###################################

filename = "tb_player_field_goals.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_player_field_goals/tb_player_field_goals.csv", "rb") as a:
    blob.upload_from_file(a)

###################################

filename = "tb_player_free_throws.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_player_free_throws/tb_player_free_throws.csv", "rb") as a:
    blob.upload_from_file(a)

###################################

filename = "tb_player_info.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_player_info/tb_player_info.csv", "rb") as a:
    blob.upload_from_file(a)

###################################

filename = "tb_players.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_players/tb_players.csv", "rb") as a:
    blob.upload_from_file(a)

###################################

filename = "tb_player_three_pointers.csv"
blob = bucket.blob(filename)
with open("data/tables/tb_player_three_pointers/tb_player_three_pointers.csv", "rb") as a:
    blob.upload_from_file(a)
```

#### 4.2 step: Upload to Google Cloud BigQuery
The below code is used to import data from the CSV files into the Google BigQuery tables. The if_exists parameter is used to specify whether the table should be replaced if it already exists. The credentials are used to authenticate the user in order to access the BigQuery tables.

code below...

```ruby
# Try to import the necessary modules (pandas, google.cloud and google.oauth2). 
# If some module is missing, print an error message
try:
    import pandas as pd
    from google.cloud import bigquery
    from google.oauth2 import service_account
except Exception as e:
    print("Some Modules are Missing {}".format(e))

# Load the data from the CSV files
tb_player_defense = pd.read_csv("data/tables/tb_player_defense/tb_player_defense.csv")
tb_player_field_goals = pd.read_csv("data/tables/tb_player_field_goals/tb_player_field_goals.csv")
tb_player_free_throws = pd.read_csv("data/tables/tb_player_free_throws/tb_player_free_throws.csv")
tb_player_info = pd.read_csv("data/tables/tb_player_info/tb_player_info.csv")
tb_player_three_pointers = pd.read_csv("data/tables/tb_player_three_pointers/tb_player_three_pointers.csv")
tb_players = pd.read_csv("data/tables/tb_players/tb_players.csv")


# Set the path for the service account key
# Set the credentials of the service account
key_path = "/home/marcelo/GCP/nba-projeto-bulk-12f049d54bf9.json"
credentials = service_account.Credentials.from_service_account_file(key_path)

# Upload the data to Google BigQuery, 
# replacing the data if the table already exists
tb_player_defense.to_gbq(credentials=credentials,
destination_table="nba.tb_player_defense", if_exists='replace')
tb_player_field_goals.to_gbq(credentials=credentials,
destination_table="nba.tb_player_field_goals", if_exists='replace')
tb_player_free_throws.to_gbq(credentials=credentials,
destination_table="nba.tb_player_free_throws", if_exists='replace')
tb_player_info.to_gbq(credentials=credentials,
destination_table="nba.tb_player_info", if_exists='replace')
tb_player_three_pointers.to_gbq(credentials=credentials,
destination_table="nba.tb_player_three_pointers", if_exists='replace')
tb_players.to_gbq(credentials=credentials,
destination_table="nba.tb_players", if_exists='replace')
```

#### 5 step: Shell Script to automate everything
This script is used to scrape and download statistics of NBA players from a website, extract, clean and save the data in tables and finally upload the data to GCP Storage and BigQuery. It starts by running the url_download.py script to scrape the website and download the data. It then runs the ETL.py script to extract, transform and load the data into different tables. Then it changes the filenames of the tables to upload to GCP. Finally, the user is asked to choose an option to upload the data to GCP Storage, BigQuery or both. Depending on the option chosen, the appropriate script is run.

code below...

```ruby
#!/bin/bash
######################################################
clear

cd Code/

echo 'scraping the URL and downloading statistics of NBA players ...'
sleep 3
python url_download.py
clear
######################################################

echo 'Extracting, Cleaning and Saving the data in tables ...'
sleep 3
python ETL.py -f
clear
 
######################################################

echo 'Changing file names to upload to GCP ....'
sleep 3

cd data/tables/tb_player_defense
for filename in *.csv
do
  mv "$filename" "tb_player_defense.csv"
done

cd ..
cd tb_player_field_goals
for filename in *.csv
do
  mv "$filename" "tb_player_field_goals.csv"
done

cd ..
cd tb_player_free_throws
for filename in *.csv
do
  mv "$filename" "tb_player_free_throws.csv"
done

cd ..
cd tb_player_info
for filename in *.csv
do
  mv "$filename" "tb_player_info.csv"
done

cd ..
cd tb_players
for filename in *.csv
do
  mv "$filename" "tb_players.csv"
done
  
cd ..
cd tb_player_three_pointers
for filename in *.csv
do
  mv "$filename" "tb_player_three_pointers.csv"

clear
######################################################

sleep 3
cd ..
cd ..
cd ..

echo '''
1 - Upload to GCP Storage.
2 - Upload to GCP BigQuery.
3 - Upload to both.

Select the option (1, 2, 3): '''
read option

while [ $option != "1" ] && [ $option != "2" ] && [ $option != "3" ]
do
	clear
	echo 'Invalid Choice..'
	echo '''
1 - Upload to GCP Storage.
2 - Upload to GCP BigQuery.
3 - Upload to both.

Select the option (1, 2, 3): '''
	read option
done

if [ $option = "1" ]
then
	python upload_storage.py
	clear
	echo 'Upload to Storage Complete !!'
elif [ $option = "2" ]
then
	python upload_bigquery.py
	clear
	echo 'Upload to Storage BigQuery Complete !!'
elif [ $option = "3" ] 
then
	python upload_storage.py
	python upload_bigquery.py
	echo 'Upload Completo !!'
fi

done
```
## How were the tables in Google Cloud:

### BigQuery
![Semantic description of image](/Images/BigQuery.png "BigQuery") 

### Storage
![Semantic description of image](/Images/Storage.png "Storage")


